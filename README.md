# Spike Report

## SPIKE TITLE - Unreal Engine Gamplay Framework

### Introduction

This spike report details how to construct a basic project in Unreal that explores each of it's areas
and detail the purpose of and relationships between each area in this Unreal Project

### Goals

1. Become acquainted with the Unreal Engine Gameplay Framework
2. Learn how GameModes, GameStates and PlayerStates work
3. Learn how Pawns and Characters work
4. Enhance existing knowledge of Controllers and AIControllers

### Personnel

* Primary - Matthew Louden
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Unreal Engine Gameplay Framework] (https://docs.unrealengine.com/en-US/Gameplay/Framework/index.html)
* [Unreal Engine GameMode and GameState] (https://docs.unrealengine.com/en-US/Gameplay/Framework/GameMode/index.html)
* [Unreal Engine Pawns] (https://docs.unrealengine.com/en-US/Gameplay/Framework/Pawn/index.html)
* [Unreal Engine Character] (https://docs.unrealengine.com/en-US/Gameplay/Framework/Pawn/Character/index.html)
* [Unreal Engine Controllers] (https://docs.unrealengine.com/en-US/Gameplay/Framework/Controller/index.html)
* [Unreal Engine Camera] (https://docs.unrealengine.com/en-US/Gameplay/Framework/Camera/index.html)
* [How to set up timers in UE4] (https://www.youtube.com/watch?v=UR4l_tsYcqs)

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Generated New UE4 Project worldspace (Sidescroller)
	1. [set up title screen level & playspace level] (https://docs.unrealengine.com/en-US/Engine/Levels/index.html#:~:text=In%20Unreal%20Engine%204%20terms,that%20contain%20a%20few%20Actors.)
		2. design the playable level
		2. [set up token spawnpoints] (https://docs.unrealengine.com/en-US/Gameplay/Framework/Pawn/index.html) (using targets)
		2. set up controller logic
	1. [set up gamestate] (https://docs.unrealengine.com/en-US/Gameplay/Framework/GameMode/index.html) (gameplay loop)
		2. set up [dual timer system] (https://www.youtube.com/watch?v=UR4l_tsYcqs) (one counting down (replenishable from tokens) + one counting up (keeps a track of how long the player lasts)
		2. [set up user interface for gameplay] (https://docs.unrealengine.com/en-US/Engine/UMG/HowTo/CreatingWidgets/index.html) (replenishing countdown timer)
		2. set up time token blueprints (add time to player's countdown clock)
	1. use an [array] (https://docs.unrealengine.com/en-US/Engine/Blueprints/UserGuide/Arrays/index.html) to facilitate spawning of time tokens in level
	1. set up 'game over' screen using user interface blueprints (launch player back to titlescreen at the click of a button)


### What we found out

How to set up and interface with dual timer systems in Unreal Engine 4

